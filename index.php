<?php

ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);

//TODO OBSERVER IMPLEMENTATION

require_once('Model/Emitter.php');

$emitter = Emitter::getInstance();
$emitter->on('user.login', function ($firstname, $lastname) {
    echo $firstname . ' ' . $lastname . ' s\'est connecté <br/>';
});

$emitter->on('user.see_profile', function ($firstname, $lastname) {
    echo $firstname . ' ' . $lastname . ' a vu votre profil <br/>';
});

$emitter->on('user.logout', function ($firstname, $lastname) {
    echo $firstname . ' ' . $lastname . ' s\'est déconnecté <br/>';
});


$emitter->emit('user.login', 'David', 'Rigaudie');
$emitter->emit('user.see_profile', 'David', 'Rigaudie');
$emitter->emit('user.logout', 'David', 'Rigaudie');

$emitter->detach("user.logout");
